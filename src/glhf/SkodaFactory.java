package glhf;

public class SkodaFactory implements AbstractFactory{
    private String model;
    private CarColor color;
    private double engineVolume;
    @Override
    public Car buildCar() {
        return new Skoda(model, engineVolume, color);
    }

}
